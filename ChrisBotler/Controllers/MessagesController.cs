﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Bot.Connector;
using Microsoft.ProjectOxford.Emotion;
using Microsoft.ProjectOxford.Emotion.Contract;
using Newtonsoft.Json.Converters;

namespace ChrisBotler.Controllers
{
    public class SampleConverter : CustomCreationConverter<Activity>
    {
        public override Activity Create(Type objectType)
        {
            return new Activity();
        }
    }

    [BotAuthentication]
    public class MessagesController : ApiController
    {
        /// <summary>
        /// POST: api/Messages
        /// Receive a message from a user and reply to it
        /// </summary>
        public async Task<HttpResponseMessage> Post([FromBody]Activity activity)
        {
            if (activity.Type == ActivityTypes.Message)
            {
                ConnectorClient connector = new ConnectorClient(new Uri(activity.ServiceUrl));
                // calculate something for us to return

                /*string activityJson = JsonConvert.SerializeObject(activity);
                Activity replyActivity = activity.CreateReply($"Your activity reply was {activityJson}");
                await connector.Conversations.ReplyToActivityAsync(replyActivity);*/

                int length = (activity.Text ?? string.Empty).Length;

                // return our reply to the user
                if (length > 0)
                {
                    Activity reply = activity.CreateReply($"You sent {activity.Text} which was {length} characters");
                    await connector.Conversations.ReplyToActivityAsync(reply);
                }

                if (activity.Attachments != null && activity.Attachments.Any())
                {
                    foreach (Attachment attachment in activity.Attachments)
                    {
                        EmotionServiceClient emotionServiceClient = new EmotionServiceClient("d8e154b28d474d26a0ac32d29e367874");
                        
                        Emotion[] emotions = attachment.ContentUrl != null
                            ? await emotionServiceClient.RecognizeAsync(attachment.ContentUrl)
                            : await emotionServiceClient.RecognizeAsync(SerializeToStream(attachment.Content));

                        if (emotions.Length <= 0)
                        {
                            continue;
                        }

                        IEnumerable<string> emotionNames = emotions.Select(emotion => emotion.Scores.ToRankedList().First().Key);

                        emotionNames.ToList().ForEach(async emotionName =>
                        {
                            Activity replyForAttachment =
                                activity.CreateReply($"You sent an emotion named {emotionName}");
                            await connector.Conversations.ReplyToActivityAsync(replyForAttachment);
                        });
                    }
                }
            }
            else
            {
                HandleSystemMessage(activity.Type);
            }
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            return response;
        }

        public static MemoryStream SerializeToStream(object o)
        {
            MemoryStream stream = new MemoryStream();
            IFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, o);
            return stream;
        }

        private static Activity HandleSystemMessage(string activityType)
        {
            switch (activityType)
            {
                case ActivityTypes.DeleteUserData:
                    // Implement user deletion here
                    // If we handle user deletion, return a real message
                    break;
                case ActivityTypes.ConversationUpdate:
                    // Handle conversation state changes, like members being added and removed
                    // Use Activity.MembersAdded and Activity.MembersRemoved and Activity.Action for info
                    // Not available in all channels
                    break;
                case ActivityTypes.ContactRelationUpdate:
                    // Handle add/remove from contact lists
                    // Activity.From + Activity.Action represent what happened
                    break;
                case ActivityTypes.Typing:
                    // Handle knowing tha the user is typing
                    break;
                case ActivityTypes.Ping:
                    break;
            }

            return null;
        }
    }
}